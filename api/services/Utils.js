module.exports = {
    fromCharString2String: function(data) {
        if (!data) return '';
        var chars = data.split('&#');
        var result = '';
        for (var i = 0; i < chars.length; i++) {
            var ch = chars[i];
            if (ch.trim()) {
                result = result + String.fromCharCode(ch.trim());
            }
        }
        return result;
    },

    html2String: function(data) {
        var newData = data.replace(/(<br>)/igm, ' \r\n');
        newData = newData.replace(/(<([^>]+)>)/igm, "");
        newData = newData.replace(/&nbsp;/igm, " ");
        newData = newData.replace(/&gt;/igm, '\>');
        newData = newData.replace(/&lt;/igm, '\<');
        return newData;
    },
    // hash the annotation
    hashAnnotation: function(txt) {
        if (!txt)
            return txt;
        txt = txt.replace(/[\s`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
        return require('crypto').createHash('md5').update(txt).digest("hex");
    },
    // hash string
    getHash: function(txt) {
        txt = txt.replace(/[\s`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
        return require('crypto').createHash('md5').update(txt).digest("hex");
    },
    //remove page from url
    stripUrl: function(url) {
        var _url = url.toLowerCase();
        if (_url.indexOf('.shtml') < 0 || _url.indexOf('.shtml') !== (_url.length - 6)) {
            return url;
        }
        var lastSlash = _url.lastIndexOf('/');
        var path = url.substring(0, lastSlash);
        return path;
    },

    // a and b are javascript Date objects
    dateDiffInDays: function(a, b) {
        var _MS_PER_DAY = 1000 * 60 * 60 * 24;
        // Discard the time and time-zone information.
        var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
        var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

        return Math.floor((utc2 - utc1) / _MS_PER_DAY);
    },
    // a and b are javascript Date objects
    dateDiffInMinutes: function(a, b) {
        var _MS_PER_MINUTE = 1000 * 60;
        // Discard the time and time-zone information.
        var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate(), a.getHours(), a.getMinutes());
        var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate(), b.getHours(), b.getMinutes());

        return Math.floor((utc2 - utc1) / _MS_PER_MINUTE);
    }
};