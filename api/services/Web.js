var http = require('http');
var iconv = require('iconv-lite');
var zlib = require('zlib');

module.exports = {

    // getCommentsPage: function(hostname, path, cookies, completed) {


    //     var _getCommentsPage = function(hostname, path, cookies, completed) {
    //         var options = {
    //             hostname: hostname,
    //             port: 80,
    //             path: '/' + path,
    //             method: 'GET',
    //             headers: {
    //                 "Accept": "image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, application/x-shockwave-flash, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/msword, application/x-ms-application, application/x-ms-xbap, application/vnd.ms-xpsdocument, application/xaml+xml, */*",
    //                 "User-Agent": "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 2.0.50727; .NET CLR 3.0.04506.648; .NET CLR 3.5.21022; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET4.0C; .NET4.0E)",
    //                 "Accept-Charset": "windows-1251",
    //                 "Accept-Encoding": "gzip, deflate",
    //                 "Pragma": "no-cache",
    //                 "Cookie": cookies
    //             }

    //         };

    //         var page = "";

    //         var req = http.request(options, function(res) {
    //             if (302 == res.statusCode) {
    //                 cookies = res.headers["set-cookie"];
    //                 _getCommentsPage(hostname, path, cookies, completed);
    //             } else
    //             if (200 == res.statusCode) {
    //                 var chunks = [];
    //                 res.on('data', function(chunk) {
    //                     chunks.push(chunk);
    //                 });
    //                 res.on('end', function() {
    //                     var encoding = res.headers['content-encoding'];
    //                     if (encoding && encoding === 'gzip') {
    //                         var buf = Buffer.concat(chunks);
    //                         zlib.unzip(buf, function(err, res) {
    //                             completed(null, iconv.decode(res, 'koi8-r'));
    //                         });
    //                     } else {
    //                         var decodedBody = iconv.decode(Buffer.concat(chunks), 'koi8-r');
    //                         //console.log(decodedBody);
    //                         completed(null, decodedBody);
    //                     }
    //                 });
    //             } else {
    //                 completed("Error getting page. Status code=" + res.statusCode);
    //             }
    //         });

    //         req.on('error', function(e) {
    //             console.log('problem with request: ' + e.message);
    //         });

    //         req.end();

    //     };

    //     _getCommentsPage(hostname, path, cookies, completed);

    // },

    // getAuthorPage: function(hostname, path, completed) {
    //     var url = hostname + '/' + path;
    //     var page = "";
    //     http.get(url, function(res) {
    //         if (200 == res.statusCode) {
    //             var chunks = [];
    //             res.on('data', function(chunk) {
    //                 chunks.push(chunk);
    //             });
    //             res.on('end', function() {
    //                 // var decodedBody = iconv.decode(Buffer.concat(chunks), 'win1251');
    //                 // completed(null, decodedBody);
    //                 var encoding = res.headers['content-encoding'];
    //                 if (encoding && encoding === 'gzip') {
    //                     var buf = Buffer.concat(chunks);
    //                     zlib.unzip(buf, function(err, res) {
    //                         completed(null, iconv.decode(res, 'win1251'));
    //                     });
    //                 } else {
    //                     var decodedBody = iconv.decode(Buffer.concat(chunks), 'win1251');
    //                     //console.log(decodedBody);
    //                     completed(null, decodedBody);
    //                 }
    //             });
    //         } else {
    //             completed("Error getting page");
    //         }
    //     }).on('error', function(e) {
    //         completed("Got error: " + e.message);
    //     });
    // }
};