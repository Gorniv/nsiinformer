var util = require('util');

module.exports = {

    // add single comments page
    addCommentsPage: function(url, callback) {
        console.log('|>Adding comments: ' + url);
        Service.checkComments(url, callback);
    },

    // add all comments for all texts from author page
    addAuthorBooksComments: function(url, callback) {

    },

    // add author page
    addAuthor: function(url, callback) {
        Service.checkAuthor(url, callback);
    },


    /////////////////////////////////////////////////////
    // 
    ////////////////////////////////////////////////////

    checkAuthor: function(url, callback) {
        console.log('|>Checking author: ' + url);
        var detectorPath = __dirname + '/../lib/siteDetector.js';
        var siteDetector = require(detectorPath);
        var site = siteDetector.detectSite(url);
        //console.log(site.urlParts);
        if (site) {

            site.processors.author.get(site, function(error, result) {
                if (error) {
                    callback('|>ERROR: ' + error);
                } else {
                    // console.log('RESULT:');
                    // console.log('+++++++++++++++++++++++++++++++++');
                    // console.log(util.inspect(result, false, null));
                    site.storage.author.save(result, site, callback);
                }
            });
        } else {
            callback('|>ERROR. No site parsers found for ' + url);
        }
    },

    // checking comments from site
    checkComments: function(url, callback) {
        console.log('|>Checking comments: ' + url);
        var detectorPath = __dirname + '/../lib/siteDetector.js';
        var siteDetector = require(detectorPath);
        var site = siteDetector.detectSite(url);
        if (site) {
            site.processors.comments.get(site, function(error, result) {
                if (error) {
                    callback('|>ERROR: ' + error);
                } else {
                    //console.log(util.inspect(result, false, null));
                    result.checkComments = true;
                    site.storage.comments.save(result, site, callback);
                }
            });
        } else {
            callback('|>ERROR. No site parsers found for ' + url);
        }
    },

    // download comments from site without saving to storage. If user just wants to show comments from site
    downloadComments: function(url, callback) {
        console.log('|>Downloading comments: ' + url);
        var detectorPath = __dirname + '/../lib/siteDetector.js';
        var siteDetector = require(detectorPath);
        var site = siteDetector.detectSite(url);
        if (site) {
            site.processors.comments.get(site, function(error, result) {
                if (error) {
                    callback('|>ERROR: ' + error);
                } else {
                    // console.log(util.inspect(result, false, null));
                    callback(null, result);
                }
            });
        } else {
            callback('|>ERROR. No site parsers found for ' + url);
        }
    },
    // load only comments without book object from DB
    loadComments: function(url, skipComments, takeComments, sorting, callback) {
        console.log('|>Loading comments: ' + url);
        var detectorPath = __dirname + '/../lib/siteDetector.js';
        var siteDetector = require(detectorPath);
        var site = siteDetector.detectSite(url);
        if (site) {
            try {
                site.storage.comments.loadComments(site.urlParts.pathname, skipComments, takeComments, sorting, function(error, result) {
                    if (error) {
                        callback('|>ERROR: ' + error);
                    } else {
                        callback(null, result);
                    }
                });
            } catch (er) {
                callback(er);
            }
        } else {
            callback('|>ERROR. No site parsers found for ' + url);
        }
    },

    // load authors from all storages by param
    loadAuthors: function(params, callback) {
        var detectorPath = __dirname + '/../lib/siteDetector.js';
        var siteDetector = require(detectorPath);
        var storages = siteDetector.getStorages();
        var writers = [];
        if (!params) params = {};

        var callStorage = function(st, cb) {
            if (st.length === 0)
                return cb();
            var s = st.shift();
            params.siteTemplate = s.siteTemplate;
            s.author.loadBy(params, function(err, lwriters) {
                if (err)
                    console.log('|>[loadAuthors] ERROR: ' + err);
                else
                    writers.push.apply(writers, lwriters);
                callStorage(st, cb);
            });
        };
        callStorage(storages.slice(), function() {
            //console.log('|>Loaded ' + writers.length + ' authors from all storages');
            callback(null,writers);
        });
    },
    // load books from all storages by param
    loadBooks: function(params, callback) {
        var detectorPath = __dirname + '/../lib/siteDetector.js';
        var siteDetector = require(detectorPath);
        var storages = siteDetector.getStorages();
        var books = [];
        if (!params) params = {};

        var callStorage = function(st, cb) {
            if (st.length === 0)
                return cb();
            var s = st.shift();
            params.siteTemplate = s.siteTemplate;
            s.comments.loadBy(params, function(err, lbooks) {
                if (err)
                    console.log('|>[loadBooks] ERROR: ' + err);
                else
                    books.push.apply(books, lbooks);
                callStorage(st, cb);
            });
        };
        callStorage(storages.slice(), function() {
            if (books.length>0)
                console.log('|>Loaded ' + books.length + ' books from all storages');
            callback(null,books);
        });
    }
};