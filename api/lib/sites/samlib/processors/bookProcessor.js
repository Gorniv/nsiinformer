///////////////////////////////////////////
// join downloads and parse
///////////////////////////////////////////

function get(site, callback) {

    if (!site)
        return callback('[bookProcessor] ERROR Missed parameters.');
    console.log('downloading ' + site.urlParts.originalUrl);
    site.downloaders.book.download(site.urlParts.originalUrl, function(error, data) {
        if (error) {
            console.log('[bookProcessor]|>ERROR: ' + error);
            callback('[bookProcessor]|>ERROR: ' + error);
        } else {
            site.parsers.book.parse(data, site.urlParts, function(error, result) {
                if (error) {
                    console.log('[bookProcessor]|>ERROR: ' + error);
                    callback('[bookProcessor]|>ERROR: ' + error);
                } else {
                    callback(null, result);
                }
            });
        }
    });
}


exports.get = get;