// minimum interval in seconds between original site http queries (if it has limitations and blocks for rapid queries, i.e. Samlib.ru)
exports.httpQueryInterval = 15;
// updates plan according author activity
function getNewCheckDate(dateDiffInDays) {
    var hours = 0;
    if (!hours) hours = (dateDiffInDays >= 0 && dateDiffInDays <= 1) && 1;
    if (!hours) hours = (dateDiffInDays >= 2 && dateDiffInDays <= 3) && 2;
    if (!hours) hours = (dateDiffInDays >= 4 && dateDiffInDays <= 5) && 3;
    if (!hours) hours = (dateDiffInDays >= 6 && dateDiffInDays <= 10) && 4;
    if (!hours) hours = (dateDiffInDays >= 11 && dateDiffInDays <= 14) && 8;
    if (!hours) hours = (dateDiffInDays >= 15 && dateDiffInDays <= 30) && 16;
    if (!hours) hours = (dateDiffInDays >= 31 && dateDiffInDays <= 60) && 24;
    if (!hours) hours = (dateDiffInDays >= 61 && dateDiffInDays <= 90) && 36;
    if (!hours) hours = (dateDiffInDays >= 91 && dateDiffInDays <= 180) && 48;
    if (!hours) hours = (dateDiffInDays >= 181) && 72;
    if (!hours) hours = 1;
    return (new Date()).addHours(hours);
}
// exported function 
exports.calculateNextCheckTime = function(writer) {

    var lastUpdate = writer.updateDetectedAt;
    var now = new Date();
    var dateDiffInDays = Math.abs(Utils.dateDiffInDays(lastUpdate, now));
    writer.checkUpdatesAt = getNewCheckDate(dateDiffInDays);

    return writer;
}