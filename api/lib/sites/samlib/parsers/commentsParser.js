var cheerio = require('cheerio');

function parse(page, urlParts, callback) {
    $ = cheerio.load(page);
    var pageData = {};
    //console.log(urlParts);
    pageData.commentsPath = urlParts.pathname;
    pageData.path = pageData.commentsPath.substring('/comments'.length-1) + '.shtml';
    pageData.href = urlParts.protocol + '//' + urlParts.host + pageData.path;

    pageData.site = urlParts.hostname;
    pageData.siteTemplate = urlParts.siteTemplate;
    pageData.commentsHref = urlParts.protocol + '//' + urlParts.host + pageData.commentsPath;
    // title
    var title = $('title');
    pageData.title = $(title).text().split(":")[1].trim();
    // rating
    var linkItems = $('b a').first();
    pageData.rating = $(linkItems).text();
    // annotation
    var items = $('li i').first();
    pageData.annotation = $(items).text();
    pageData.annotationHtml = $(items).html();
    // pages
    //var expr = /<a href=\/comment\/.*?\?PAGE=(\d+)>/igm;
    //var e = '<a href=' + urlParts.pathname.replace('/', '\\/') + '?PAGE=(\\d+)>';
    //var e = '<a href=\/comment\/p\/pupkin_wasja_ibragimowich\/updatetxt\\?PAGE=(\\d+)>';
    //console.log(e);
    // var expr = new RegExp(e, 'gmi');
    // items = expr.exec(page);
    //     console.log('items:');
    //     console.log(items);
    // pageData.pages = 1;
    // $(items).each(function(i, item) {
    //     console.log('.');
    //     var p = parseInt(item);
    //     if (p > pageData.pages)
    //         pageData.pages = p;
    // });

    var expr = /<center><b>Страниц \((\d+)\):<\/b>/igm;
    items = expr.exec(page);
    pageData.pages = 1;
    if (items) {
        $(items).each(function(i, item) {
            var p = parseInt(item);
            if (p > pageData.pages)
                pageData.pages = p;
        });
    }

    expr = /<b>Архивы \((\d+)\):<\/b>/igm;
    items = expr.exec(page);
    pageData.archives = 0;
    if (items) {
        $(items).each(function(i, item) {
            var p = parseInt(item);
            if (p > pageData.archives)
                pageData.archives = p;
        });
    }



    //comments
    expr = /<small>(\d+)\.<\/small>\s*<b>\*?(?:<noindex><a href=\"(.*?)\"[^>]*>)?(?:<font[^>]*>)?(.*?)(?:<\/font>)?(?:<\/a><\/noindex>)?<\/b>\s*(?:\(<u>(.*?)<\/u>\)\s*)?<small><i>(.*?)\s*<\/i>\s*(?:\[<a href=\"(\S+?)\"\s*>исправить<\/a>\]\s*)?(?:\[<a href=\"(\S+?)"\s*><font[^>]*>удалить<\/font><\/a>\]\s*)?(?:\[<a href=\"(\S+?)"\s*>ответить<\/a>\]\s*)?<\/small><\/i>\s*<br>(.*?)\s*<hr noshade>/igm;

    pageData.comments = [];
    page.replace(expr, function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9) {
        pageData.comments.push({
            html: p0,
            number: p1,
            archive: pageData.archives,            
            authorUrl: p2,
            author: p3,
            email: Utils.fromCharString2String(p4),
            published: p5,
            editUrl: p6,
            deleteUrl: p7,
            replyUrl: p8,
            commentHtml: p9,
            comment: Utils.html2String(p9)
        });
    });
    callback(null, pageData);
}

exports.parse = parse;