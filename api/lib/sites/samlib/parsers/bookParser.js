var cheerio = require('cheerio');

function parse(page, urlParts, completed) {

    var result = {
        text: '',
        urlParts: urlParts
    };
    var startMarker = '<!----------- Собственно произведение --------------->';
    var endMarker = '<!--------------------------------------------------->';
    var start = page.indexOf(startMarker);
    var end = 0;
    if (start < 0) {
        result.text = page;
    } else {
        end = page.indexOf(endMarker, start);
        if (end < 0) {
            result.text = page;
        } else {
            result.text = page.substring(start + startMarker.length, end);
        }
    }

    $ = cheerio.load(result.text);
    var images = $('img');
    if (images) {
        for (var i = 0; i < images.length; i++) {
            var img = images[i.toString()];
            if (img.attribs.src.startsWith('/')) {
                var newUrl = urlParts.protocol + '//' + urlParts.host +  img.attribs.src;
                result.text = result.text.replace('src="' + img.attribs.src + '"', 'src="' + newUrl + '"');
            }
        }
    }
    completed(null, result);
}


exports.parse = parse;