/// <reference path="typings/node/node.d.ts"/>
/**
 * app.js
 *
 * Use `app.js` to run your app without `sails lift`.
 * To start the server, run: `node app.js`.
 *
 * This is handy in situations where the sails CLI is not relevant or useful.
 *
 * For example:
 *   => `node app.js`
 *   => `forever start app.js`
 *   => `node debug app.js`
 *   => `modulus deploy`
 *   => `heroku scale`
 *
 *
 * The same command-line arguments are supported, e.g.:
 * `node app.js --silent --port=80 --prod`
 */

// Ensure a "sails" can be located:
(function() {
    var sails;
    try {
        sails = require('sails');
    } catch (e) {
        console.error('To run an app using `node app.js`, you usually need to have a version of `sails` installed in the same directory as your app.');
        console.error('To do that, run `npm install sails`');
        console.error('');
        console.error('Alternatively, if you have sails installed globally (i.e. you did `npm install -g sails`), you can use `sails lift`.');
        console.error('When you run `sails lift`, your app will still use a local `./node_modules/sails` dependency if it exists,');
        console.error('but if it doesn\'t, the app will run with the global sails instead!');
        return;
    }

    // Try to get `rc` dependency
    var rc;
    try {
        rc = require('rc');
    } catch (e0) {
        try {
            rc = require('sails/node_modules/rc');
        } catch (e1) {
            console.error('Could not find dependency: `rc`.');
            console.error('Your `.sailsrc` file(s) will be ignored.');
            console.error('To resolve this, run:');
            console.error('npm install rc --save');
            rc = function() {
                return {};
            };
        }
    }

    require(__dirname + '/api/lib/extentions.js');
    // Start server
    sails.lift(rc('sails'), function(err, sails) {
        if (err) {
            console.log(err);
            return;
        }
        //Service.loadComments("http://budclub.ru/comment/p/pupkin_wasja_ibragimowich/updatetxt", 0,10,'', function(err, result) {
        //Service.addCommentsPage("http://samlib.ru/comment/p/pupkin_wasja_ibragimowich/updatetxt", function(err, result) {
        //Service.addCommentsPage("http://samlib.ru/comment//p/pupkin_wasja_ibragimowich/siinformer", function(err, result) {
        //   console.log("Result:");
        //   console.log(result);
        //Service.addCommentsPage("http://samlib.ru/comment/p/pupkin_wasja_ibragimowich/nik6_scene1", function(err, result) {
        //Service.addAuthor("http://samlib.ru/p/pupkin_wasja_ibragimowich", function(err, result) {
        //Service.addAuthor("http://samlib.ru/r/radzhapow_r_e", function(err, result) {
        //     console.log("Result:");
        //     if (result.writer){
        //        result.writer='writer object truncated for test pupoceses...';
        //     }
        //     console.log(result);
        // });


        // get all storages, get all sites and updatable authors
        // var detectorPath = __dirname + '/api/lib/siteDetector.js';        
        // var siteDetector = require(detectorPath);        
        // var storages = siteDetector.getStorages();
        // var writers = [];

        // var callStorage = function(st, cb) {
        //     if (st.length === 0)
        //         return cb();
        //     var s = st.shift();
        //     s.author.loadBy({
        //         checkUpdates: true,
        //         siteTemplate: s.siteTemplate 
        //     }, function(err, lwriters) {
        //         writers.push.apply(writers, lwriters);
        //         callStorage(st, cb);
        //     });
        // };
        // callStorage(storages.slice(), function(){
        //     console.log('|>Loaded ' + writers.length + ' authors');
        // });
        // Service.loadAuthors({}, function(err, authors){
        //     //console.log('!!!!!> authors: ' + authors.length);            
        // });

        // var signals = require(__dirname +  '/api/lib/Signals.js').Signals;
        // signals.receive("myEvent", function(dta) {
        //     console.log('>FROM APP: ' + dta); 
        // });
        //setTimeout(function(){

        require(__dirname + '/api/lib/authorsUpdatesObserver.js');
        require(__dirname + '/api/lib/commentsUpdatesObserver.js');
        //},5000);

        // var detectorPath = __dirname + '/api/lib/siteDetector.js';
        // var siteDetector = require(detectorPath);
        // var site = siteDetector.detectSite('http://samlib.ru/p/pupkin_wasja_ibragimowich/updatetxt.shtml');
        // if (site){
        //     //console.log(site);
        //     site.processors.book.get(site, function(error, result){
        //         if (error){
        //             console.log(error);
        //         }else{
        //             console.log(result.text);
        //         }
        //     });
        // }
    });



})();