require('insert-css')(require('./style.css'))
module.exports = {
    template: require('./template.html'),
    replace: true,
    data: function() {
        return {
            visible: false,
            windowWidth: 0,
            panelWidth: 0,
            panelRight: 0,
            book: {},
            commentsPage: undefined,
            listHeight: 100,
            panelExpanded: false,
            page: 1,
            pages: 1,
            pagesLeft: 1,
            pagesRight: 0,
        };
    },
    ready: function() {
        var self = this;
        self.windowWidth = window.innerWidth;
        self.panelWidth = self.windowWidth / 2;
        self.panelRight = -self.windowWidth; // hide        
        self.visible = false;
        $(window).on('resize', function() {
            self.onResize(self);
        });
        Signals.receive("showFastComments", function(book) {
            self.page = 1;
            self.book = book;
            self.showFastComments();
        });
        self.correctHeight();
    },
    created: function() {
        require('vue').component("comment", require('./../comment/index.js'));
        var self = this;
        self.commentsPage = {comments: []};
        Signals.receive("layoutUpdated", function() {
            self.onResize(self);
        });
    },
    methods: {
        onResize: function(self, delay) {
            delay = delay || 500;
            if (self.updateLayoutTimer) clearTimeout(self.updateLayoutTimer);
            self.updateLayoutTimer = setTimeout(function() {
                self.windowWidth = window.innerWidth;
                var f = $('.books-list')[0];
                if (!f) return;
                self.panelWidth = self.panelExpanded ? f.clientWidth - f.getClientRects()[0].left : self.windowWidth / 2;
                self.panelRight = self.visible ? 0 : -self.windowWidth;
                self.correctHeight();
            }, delay);
        },
        correctHeight: function() { // bulls shit!!!!!
            var fc = $('.fast-comments')[0];
            var fch = $('#commentsListHeader')[0];
            this.listHeight = fc.clientHeight - fch.clientHeight - 90;
        },
        showFastComments: function() {
            this.visible = true;
            var self = this;
            self.correctHeight();
            setTimeout(function() {
                self.panelRight = 0;
            }, 100);            
            var mprogress = new Mprogress({
                template: 3,
                parent: '#progressPlaceholder'
            });
            mprogress.start();
            window.socket.get("/book/downloadComments", {
                commentsPath: 'http://' + self.book.site + '/' + self.book.commentsPath + '?PAGE=' + self.page
            }, function gotResponse(resData, jwres) {
                if (resData.error) {
                    $utils.showDialog('Ошибка получения комментариев: ' + resData.error, 'Node SIInformer');
                    mprogress.end();
                    return;
                } else {
                    // some correction
                    _(resData.comments).forEach(function(c) {
                        // while (c.commentHtml.startsWith('&nbsp;')){
                        while (c.commentHtml.indexOf('&nbsp;') >= 0) {
                            // c.commentHtml = c.commentHtml.substring(6);
                            c.commentHtml = c.commentHtml.replace('&nbsp;', '');
                        }
                    });
                    self.commentsPage = resData;
                    self.pages = self.commentsPage.pages;
                    self.pagesLeft = [];
                    if (self.commentsPage.pages <= 10) {
                        for (var i = 1; i <= self.pages; i++) {
                            self.pagesLeft.push(i);
                        };
                    } else if (self.page > self.pages - 10) {
                        for (var i = self.commentsPage.pages - 10; i <= self.commentsPage.pages; i++) {
                            self.pagesLeft.push(i);
                        };
                    } else {
                        for (var i = self.page; i <= self.page + 10; i++) {
                            self.pagesLeft.push(i);
                        };
                    }                    
                }
                self.correctHeight();
                mprogress.end();
            });
        },
        onClose: function() {
            this.panelRight = -this.windowWidth;
            var self = this;
            setTimeout(function() {
                self.visible = false;
            }, 100);
        },
        onToggleExpand: function() {
            this.panelExpanded = !this.panelExpanded;
            this.onResize(this, 1);
        },
        onRefresh: function() {
            this.showFastComments();
        },
        isActive: function(number) {
            return number == this.page ? "active" : "";
        },
        setPage: function(newPage) {
            this.page = newPage;
            this.showFastComments();
        },
        setFirstPage: function() {
            if (this.page == 1) return;
            this.page = 1;
            this.showFastComments();
        },
        setLastPage: function() {
            if (this.page == this.pages) return;
            this.page = this.pages;
            this.showFastComments();
        },
        setPreviousPage: function() {
            if (this.page - 1 < 1) return;
            this.page = this.page - 1;
            this.showFastComments();
        },
        setNextPage: function() {
            if (this.page + 1 > this.pages) return;
            this.page = this.page + 1;
            this.showFastComments();
        },
        showMore: function() {
            return this.page < this.pages - 10;
        }
    }
}