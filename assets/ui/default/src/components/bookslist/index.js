require('insert-css')(require('./style.css'))
module.exports = {
    template: require('./template.html'),
    replace: true,
    data: function() {
        return {
            ready: false,
            author: {},
            updateDetectedAt_calendar: undefined,
            image: '',
            books: [],
            header_colors: ['#8CC152', '#37BC9B', '#3BAFDA', '#E9573F', '#F6BB42', '#434A54', '#D87979', '#B0E297', '#F0DBF7', '#97C2E2', '#E2DD97', '#97E2DF'],
            lastGroup: '',
            colorIndex: 0
        };
    },
    ready: function() {
        this.scroller = new IScroll('#scroller-wrapper', {
            scrollX: true,
            scrollY: false,
            mouseWheel: true,
            scrollbars: true,
            keyBindings: true,
            interactiveScrollbars: true
        });
        theme_init.tooltips();
        this.timer_based_refresh();
    },
    created: function() {
        var Vue = require('vue');
        Vue.component('fastcomments', require('./../fastcomments/index.js'));
        Vue.component("book", require('./../book/index.js'));

        $states.booksViewType = 'unread';
        this.$on('author_selected', this.author_selected);
        this.$on('author_selected_forcerefresh', function(author) {
            this.author_selected(author, true);
        });
        var self = this;
        // didn't like vuejs native messages broadcasting. Will use Signals
        Signals.receive("updateBooksLayout", function() {
            self.timer_based_refresh();
        });
        db.books.on('add', function(book) {
            self.updateBook(book.toJSON());
            self.timer_based_refresh();
        });
        // after change book in backbone collection
        db.books.on('change', function(bookObj) {
            self.updateBook(bookObj.toJSON());
        });
        // if book removed from backbone collection
        db.books.on('remove', function(bookObj) {
            var book = bookObj.toJSON();
            self.removeBook(book);
            self.timer_based_refresh();
        });
        $(window).on('resize', function() {
            self.timer_based_refresh();
        });
    },
    methods: {
        // to make layout refresh as less as possible
        timer_based_refresh: function() {
            if (this.updateLayoutTimer) clearTimeout(this.updateLayoutTimer);
            this.updateLayoutTimer = setTimeout(this.update_layout, 100);
        },
        // author selected from list
        author_selected: function(author, force_refresh) {
            if (!force_refresh) {
                if (this.author && this.author.id == author.id && this.author.hasUpdates == author.hasUpdates) { // do not update books if author already selected (fix of not working preventDefault on author menu-button pressing)
                    return;
                }
            } else {
                if (this.author && this.author.id != author.id) { // do not update books if updated author is not we are
                    return;
                }
            }
            $states.booksGroups = [];
            this.author = $states.selectedAuthor = author;
            this.updateDetectedAt_calendar = moment(author.updateDetectedAt).calendar().toLowerCase();
            this.image = $utils.getAuthorImage(author);
            this.books = [];

            if (!this.mprogress){
                this.mprogress = new Mprogress({
                    template: 3,
                    parent: '#scroller-wrapper'
                });
            }
            this.mprogress.start();

            // load author books from server
            console.log('fetching books for writer ' + author.name);
            db.books.reset();
            db.books.fetch({
                data: {
                    writer: author.id
                }
            });
            this.update_layout();
            this.ready = true;
            if (!force_refresh) this.scroller.scrollTo(0, 0);
        },
        update_layout: function() {
            var wall = $('#booksScrollerDiv')[0];
            if (!wall) return;
            var items = wall.children;
            console.log('updating ' + items.length + ' items');
            var containerHeight = wall.clientHeight;
            var colWidth = 400;
            var gap = 15;
            wall.style.width = colWidth;
            var col = 0;
            var rowHeight = gap / 2;
            _(items).forEach(function(item) {
                if (item.style.display !== 'none') {
                    if (rowHeight + item.clientHeight > containerHeight) {
                        col = col + 1;
                        rowHeight = gap / 2;
                    }
                    if (item.clientHeight > containerHeight) {
                        var textContainer = $(item).find('.annotation');
                        textContainer[0].style.height = containerHeight - gap * 2 - 200 + 'px';
                        textContainer[0].style.overflow = 'hidden';
                    }
                    var left = (col) * (colWidth + gap / 2) + gap;
                    item.style.left = left + 'px';
                    item.style.top = rowHeight + 'px';
                    rowHeight = rowHeight + item.clientHeight + gap;
                }
            });
            wall.style.width = (col) * (colWidth) + colWidth * 3 + 'px';
            this.scroller.refresh();
        },
        // book updated in backbone collection
        updateBook: function(book) {
            // if other book updated, not of selected author - do nothing
            if (this.author.id !== book.writer) {
                return;
            }

            if (this.mprogress){
                this.mprogress.end();
            }

            var index = _.findIndex(this.books, {
                id: book.id
            });
            if (index >= 0) {
                console.log('>updated ' + book.title + ' with "hasUpdates" = ' + book.hasUpdates);
                var localBook = this.books[index];
                for (var attr in book) {
                    if (book.hasOwnProperty(attr)) localBook.$set(attr, book[attr]);
                }
            } else {
                var groupIndex = _.findIndex($states.booksGroups, {
                    group: book.group
                });
                if (this.lastGroup !== book.group) {
                    this.lastGroup = book.group;
                    this.colorIndex = this.colorIndex + 1;
                    if (this.colorIndex >= this.header_colors.length) {
                        this.colorIndex = 0;
                    }
                    book.isNewGroup = true;
                    if (groupIndex < 0) {
                        $states.booksGroups.push({
                            group: book.group,
                            expanded: book.hasUpdates,
                            books_count: 1,
                            books_updated_count: book.hasUpdates ? 1 : 0
                        });
                    }
                } else { // check if we have updated book inside group not in first position
                    if (groupIndex >= 0) {
                        $states.booksGroups[groupIndex].books_count = $states.booksGroups[groupIndex].books_count + 1;
                        $states.booksGroups[groupIndex].books_updated_count = book.hasUpdates ? $states.booksGroups[groupIndex].books_updated_count + 1 : $states.booksGroups[groupIndex].books_updated_count;
                        if (!$states.booksGroups[groupIndex].expanded && book.hasUpdates) {
                            $states.booksGroups[groupIndex].expanded = book.hasUpdates;
                        }
                    }
                }
                book.header_color = this.header_colors[this.colorIndex];
                this.books.push(book);
            }
        },
        // book removed from backbone collection
        removeBook: function(book) {
            var index = _.findIndex(this.books, {
                id: book.id
            });
            if (index >= 0) {
                //console.log('>removed ' + book.title);
                this.books.$remove(index);
            }
        },
        onViewTypeChanged: function(event) {
            $states.booksViewType = event.target.value;
            Signals.signal("booksViewTypeChanged");
            Signals.signal('updateBooksLayout');
        }
    }
}